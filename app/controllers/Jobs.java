package controllers;

import models.Job;
import play.data.Form;
import play.db.ebean.Transactional;
import play.mvc.Controller;
import play.mvc.Result;

public class Jobs extends Controller {

	public static final Form<Job> jobForm = Form.form(Job.class);
	
	@Transactional
	public static Result list() {
		return ok(views.html.job.all.render(Job.find.all()));
	}

	public static Result show(long id) {
		Job job = Job.find.byId(id);
		if (job == null) {
			flash("error", String.format("No job with id %d!", id));
			return list();
		}
		return ok(views.html.job.single.render(job));
	}
	
	public static Result save() {
		Form<Job> filledForm = jobForm.bindFromRequest();
		Job created = filledForm.get();
		created.save();
		flash("success", "The job was created successfully!");
		return list();
	}

	public static Result create() {
		return ok(views.html.job.create.render(new Job()));
	}
}
