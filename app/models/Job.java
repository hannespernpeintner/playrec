package models;

import javax.persistence.Entity;
import javax.persistence.Id;

import play.db.ebean.Model;

@Entity
public class Job extends Model {
	
	@Id
	private long id;
	private String title = "";
	private String description = "";

	public long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getShortDescription(int length) {
		int resultLength = description.length();
		return resultLength < length ? description : description.substring(0,  length);
	}

	public static Finder<Long, Job> find = new Finder<Long, Job>(Long.class, Job.class);

}
