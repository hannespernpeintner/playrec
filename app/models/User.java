package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

import play.db.ebean.Model;
import views.html.job.all;

@Entity
public class User extends Model {
	
	@Id
	private long id;
	
	private String email;
	
	public User(String email, String password) {
		this.email = email;
		this.password = password;
	}
	
	public User(){}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public long getId() {
		return id;
	}
	private String password;

	public static User authenticate(String email, String password) {
		createIfNoUsers(email, password);
		
		List<User> usersForEmail = find.where().ilike("email", email).findList();
		
		if(usersForEmail.isEmpty()) {
			return null;
		}
		
		User user = usersForEmail.get(0);
		if (user.getPassword().equals(password)) {
			return user;
		}
		
		return null;
	}
	
	private static void createIfNoUsers(String email, String password) {
		List<User> allUsers = find.all();
		if (allUsers.isEmpty()) {
			new User(email, password).save();
		}
	}
	public static Finder<Long, User> find = new Finder<Long, User>(Long.class, User.class);

}
