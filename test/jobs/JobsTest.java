package jobs;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.HTMLUNIT;
import static play.test.Helpers.running;
import static play.test.Helpers.testServer;

import org.junit.Test;

import play.libs.F.Callback;
import play.test.TestBrowser;

public class JobsTest {
	@Test
	public void allJobs() {
	    running(testServer(3333), HTMLUNIT, new Callback<TestBrowser>() {
	        public void invoke(TestBrowser browser) {
	            browser.goTo("http://localhost:3333/jobs");
	            assertThat(browser.title()).isEqualTo("Jobs");
	            assertThat(browser.pageSource()).contains("All jobs");
	        }
	    });
	}
}
